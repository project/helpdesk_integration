This is a framework module. It provides the foundation for integrating your
Drupal site with a dedicated helpdesk system such as Zendesk, Zammad, GitLab
service desk, or others.

The idea behind that integration is, that users of a Drupal site, e.g. a portal,
an intranet, or any other web application, already may have user accounts on
such a site and use it for the purpose of that site. If the owner of that site
wants to offer some helpdesk as well, the best user experience is to keep the
users inside the application, i.e. on the Drupal site, and let them use it as
their user front end to submit new issues and follow the progress of their
issues until they get resolved.

While this is a great UX for end users, it would not be ideal for your support
agents, as dedicated helpdesk systems offer a lot of specialized functionality
that makes the agent's daily work much more convenient and efficient.

## Getting the best UX for all participants

With this approach, you're getting the best UX for both, your users and your
support agents. The users can stay within the application and don't have to
worry about a separate user account, login or even the platform that's used by
your agents, as that's something completely irrelevant to end users.

At the same time, your agents get the great tool from a dedicated helpdesk
system which provides them with all the functionality they need. And most of
the dedicated helpdesk system also come with more integrations like e.g. phone
support and others.

## How does it work

As a Drupal site owner, you can configure one or many integrations with the
supported dedicated helpdesk platforms by installing the required modules and
then going to `/admin/config/services/helpdesk` to create and setup the
integration(s).

**Supported Dedicated Helpdesk Platforms**

- [GitLab](https://www.drupal.org/project/helpdesk_gitlab)
- [Zammad](https://www.drupal.org/project/helpdesk_zammad)
- [Zendesk](https://www.drupal.org/project/helpdesk_zendesk)

The configuration of each integration depends on the platform and is described
for each of them on their project's main pages or documentation.

**User account synchronization**

Drupal will automatically synchronize user accounts from the Drupal site into
the helpdesk system, so that issues can be associated with the correct user and
that the agents working in the helpdesk system also know, with whom they
communicate. Only user accounts with the permission to use the helpdesk
feature on the Drupal site will be synchronized.

**Data storage**

The data for issues, comments, attachments, issue status, etc. is owned and
controlled by the helpdesk system. For better user experience on the Drupal site
and for best performance, the relevant issue data is synchronized over to the
Drupal site in the background. This happens only when needed, not all of the
data from the helpdesk system will be available in Drupal as well.

To store that data, this module provides a new content type with comments and
attachments. For comments, there is a dedicated comment bundle to the comment
content type from Drupal core.

**Usage**

This module provides a new route at `/helpdesk` which is accessible by all
permitted users. You can define a path alias if you want that to be available
at a different route. And, of course, you can add that to a menu on the Drupal
site, so that users can easily navigate there.

On that page, each user can see their own open issues, create new one, comment
to existing ones, upload additional attachments or mark an issue as resolved.
