<?php

namespace Drupal\helpdesk_integration;

use Drupal\Component\Plugin\Exception\ExceptionInterface;

/**
 * Exception class for handling the errors in helpdesk integration.
 *
 * @package Drupal\helpdesk_integration
 */
class HelpdeskPluginException extends \Exception implements ExceptionInterface {}
