<?php

namespace Drupal\helpdesk_integration;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginBase as CorePluginBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\Event\FileUploadSanitizeNameEvent;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\comment\CommentInterface;
use Drupal\comment\Entity\Comment;
use Drupal\file\Entity\File;
use Drupal\file\Upload\FileUploadHandler;
use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Base class for helpdesk_integration plugins.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface, ContainerFactoryPluginInterface {

  /**
   * The client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected ClientFactory $clientFactory;

  /**
   * The helpdesk service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The user data.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected UserDataInterface $userData;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The type guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected MimeTypeGuesserInterface $mimeTypeGuesser;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * PluginBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   HTTP client factory.
   * @param \Drupal\helpdesk_integration\Service $service
   *   Helpdesk services.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data service to persistently store data for each user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Drupal's time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Drupal's date formatter service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Drupal's logger service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The currently logged-in user account session.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mime_type_guesser
   *   The mime type guesser.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  final public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientFactory $client_factory, Service $service, EntityTypeManagerInterface $entity_type_manager, UserDataInterface $user_data, TimeInterface $time, DateFormatterInterface $date_formatter, LoggerInterface $logger, AccountInterface $current_user, FileSystemInterface $file_system, MimeTypeGuesserInterface $mime_type_guesser, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->clientFactory = $client_factory;
    $this->service = $service;
    $this->entityTypeManager = $entity_type_manager;
    $this->userData = $user_data;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->logger = $logger;
    $this->currentUser = $current_user;
    $this->fileSystem = $file_system;
    $this->mimeTypeGuesser = $mime_type_guesser;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ContainerFactoryPluginInterface|PluginBase|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client_factory'),
      $container->get('helpdesk_integration.service'),
      $container->get('entity_type.manager'),
      $container->get('user.data'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('logger.factory')->get('helpdesk'),
      $container->get('current_user'),
      $container->get('file_system'),
      $container->get('file.mime_type.guesser'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function findUserByData(HelpdeskInterface $helpdesk, string $key, $data): ?UserInterface {
    static $users = [];
    $hash = is_scalar($data) ? $data : hash('md5', strval($data));
    if (empty($users[$helpdesk->id()][$key][$hash])) {
      $load_uid = 1;
      $data_key = implode(' ', [$helpdesk->id(), $key]);
      foreach ($this->userData->get('helpdesk_integration', NULL, $data_key) as $uid => $value) {
        if ($value === $data) {
          $load_uid = $uid;
          break;
        }
      }
      try {
        $users[$helpdesk->id()][$key][$hash] =
          $this->entityTypeManager->getStorage('user')
            ->load($load_uid);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
        $this->logger->error($e->getMessage());
      }
    }
    return $users[$helpdesk->id()][$key][$hash];
  }

  /**
   * {@inheritdoc}
   */
  public function getUserData(UserInterface $user, HelpdeskInterface $helpdesk, string $key): mixed {
    $data_key = implode(' ', [$helpdesk->id(), $key]);
    return $this->userData->get('helpdesk_integration', $user->id(), $data_key);
  }

  /**
   * {@inheritdoc}
   */
  public function setUserData(UserInterface $user, HelpdeskInterface $helpdesk, string $key, mixed $value): void {
    $data_key = implode(' ', [$helpdesk->id(), $key]);
    $this->userData->set('helpdesk_integration', $user->id(), $data_key, $value);
  }

  /**
   * {@inheritdoc}
   */
  final public function updateAllIssues(HelpdeskInterface $helpdesk, ?UserInterface $user = NULL, $force = FALSE): void {
    try {
      if ($user === NULL) {
        $user = $this->entityTypeManager->getStorage('user')
          ->load($this->currentUser->id());
      }
      $last_sync = (int) $this->getUserData($user, $helpdesk, 'last_sync');
      $current = $this->time->getRequestTime();
      if (!$force && ($last_sync + self::REFRESH_INTERVAL >= $current)) {
        return;
      }
      $this->setUserData($user, $helpdesk, 'last_sync', $current);

      $issueStorage = $this->entityTypeManager->getStorage('helpdesk_issue');
      $commentStorage = $this->entityTypeManager->getStorage('comment');

      $remoteIssueIds = [];
      foreach ($this->getAllIssues($helpdesk, $user, $last_sync) as $issue) {
        $localIssueId = $this->synchronizeIssues($issueStorage, $helpdesk, $issue, $user);
        $remoteIssueIds[] = $localIssueId;

        $remoteCommentIds = [];
        foreach ($issue->getComments() as $comment) {
          $remoteCommentIds[] = $this->synchronizeComments($commentStorage, $localIssueId, $comment);
        }
        $this->removeExistingLocalComments($commentStorage, $localIssueId, $remoteCommentIds);
      }
      $this->removeAccessOfExistingItems($helpdesk, $user, $remoteIssueIds, $last_sync);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException | HelpdeskPluginException | EntityStorageException $e) {
      throw new HelpdeskPluginException($e);
    }
  }

  /**
   * Synchronize the issues.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $issueStorage
   *   The issue storage.
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue.
   * @param \Drupal\Core\Entity\EntityInterface|null $user
   *   The user.
   *
   * @return int
   *   The local Issue ID.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function synchronizeIssues(EntityStorageInterface $issueStorage, HelpdeskInterface $helpdesk, IssueInterface $issue, ?EntityInterface $user): int {
    $changed = FALSE;
    if ($localIssues = $issueStorage->loadByProperties([
      'helpdesk' => $helpdesk->id(),
      'extid' => $issue->get('extid')->value,
    ])) {
      /** @var \Drupal\helpdesk_integration\IssueInterface $localIssue */
      $localIssue = array_shift($localIssues);
      foreach ([
        'resolved',
        'title',
        'status',
        'changed',
        'body',
      ] as $field_name) {
        if ($localIssue->get($field_name)->value !== $issue->get($field_name)->value) {
          $localIssue->set($field_name, $issue->get($field_name)->value);
          $changed = TRUE;
        }
      }
    }
    else {
      $issue->set('attachments',
        $this->downloadAndGetAttachmentValues($issue->getIssueAttachments()));
      $localIssue = $issue;
      $changed = TRUE;
    }
    try {
      if ($localIssue->addUser($user->id()) || $changed) {
        $localIssue->save();
      }
    }
    catch (EntityStorageException $e) {
      throw new HelpdeskPluginException($e);
    }
    return $localIssue->id();
  }

  /**
   * Synchronize the comments.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $commentStorage
   *   The comment storage.
   * @param int $localIssueId
   *   The local Issue ID.
   * @param array $comment
   *   The remote comment.
   *
   * @return int
   *   The comment ID.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function synchronizeComments(EntityStorageInterface $commentStorage, int $localIssueId, array $comment): int {
    if ($localComments = $commentStorage->loadByProperties([
      'entity_type' => 'helpdesk_issue',
      'entity_id' => $localIssueId,
      'field_extid' => $comment['extid'],
    ])) {
      /** @var \Drupal\comment\CommentInterface $localComment */
      $localComment = array_shift($localComments);
      $this->updateCommentIfNecessary($localComment, $comment);
    }
    else {
      $localComment = Comment::create([
        'entity_type' => 'helpdesk_issue',
        'entity_id' => $localIssueId,
        'comment_type' => 'helpdesk_issue_comment',
        'field_name' => 'comment',
        'field_extid' => $comment['extid'],
        'field_deleted' => 0,
        'uid' => $comment['author']->id(),
        'created' => $comment['created'],
        'changed' => $comment['changed'],
        'comment_body' => [
          'value' => $comment['body'],
          'format' => 'basic_html',
        ],
        'field_attachments' => $this->downloadAndGetAttachmentValues($comment['attachments']),
      ]);
      $localComment->save();
    }

    return $localComment->id();
  }

  /**
   * Update comment if necessary.
   *
   * @param \Drupal\comment\CommentInterface $localComment
   *   The local comment.
   * @param array $comment
   *   The remote comment.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function updateCommentIfNecessary(CommentInterface $localComment, array $comment): void {
    $changed = FALSE;
    foreach ([
      'comment_body' => 'body',
      'created' => 'created',
      'changed' => 'changed',
    ] as $local_field_name => $remote_field_name) {
      if ($localComment->get($local_field_name)->value != $comment[$remote_field_name]) {
        $localComment->set($local_field_name, $comment[$remote_field_name]);
        $changed = TRUE;
      }
      if ($localComment->getOwner()->id() != $comment['author']->id()) {
        $localComment->setOwnerId($comment['author']);
        $changed = TRUE;
      }
    }
    if ((int) $localComment->get('field_deleted')->value === 1) {
      $localComment->set('field_deleted', 0);
      $changed = TRUE;
    }
    if ($changed) {
      try {
        $localComment->save();
      }
      catch (EntityStorageException $e) {
        throw new HelpdeskPluginException($e);
      }
    }
  }

  /**
   * Get all attachment values, if present.
   *
   * @param array $attachments
   *   The attachments.
   *
   * @return array
   *   The attachment values with file information.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function downloadAndGetAttachmentValues(array $attachments): array {
    $attachmentValues = [];
    $directory = self::HELPDESK_DIR;
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    try {
      foreach ($attachments as $attachment) {
        $fileName = $this->sanitizeFileName($attachment['file_name']);
        $download = 'temporary://' . basename($fileName);
        $response = $this->clientFactory->fromOptions([
          'base_uri' => $attachment['url'],
          'headers' => $attachment['download_headers'] ?? [],
        ])->request($attachment['method'] ?? 'get');
        file_put_contents($download, $response->getBody()->getContents());

        $values = [
          'uid' => 1,
          'status' => 1,
          'filename' => basename($download),
          'uri' => $download,
          'filesize' => filesize($download),
          'filemime' => $this->mimeTypeGuesser->guessMimeType($download),
        ];
        $file = File::create($values);
        if ($filename = $this->fileSystem->move($download, $directory)) {
          $file->setFileUri($filename);
          if ($this->fileSystem->chmod($file->getFileUri()) && $file->save()) {
            $attachmentValues[] = [
              'target_id' => $file->id(),
              'description' => $fileName,
            ];
          }
        }
      }
    }
    catch (EntityStorageException | GuzzleException $e) {
      throw new HelpdeskPluginException($e);
    }
    return $attachmentValues;
  }

  /**
   * Sanitizes the file name.
   *
   * @param string $filename
   *   The file name.
   *
   * @return string
   *   The sanitized file name.
   */
  private function sanitizeFileName(string $filename): string {
    $event = new FileUploadSanitizeNameEvent($filename, FileUploadHandler::DEFAULT_EXTENSIONS);
    $this->eventDispatcher->dispatch($event);
    return $event->getFilename();
  }

  /**
   * Remove existing local comments that no longer exist remotely.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $commentStorage
   *   The comment storage.
   * @param int $localIssueId
   *   The local issue ID.
   * @param array $remoteCommentIds
   *   The IDs for remote issues.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function removeExistingLocalComments(EntityStorageInterface $commentStorage, int $localIssueId, array $remoteCommentIds): void {
    /** @var \Drupal\comment\Entity\Comment[] $localComments */
    $localComments = $commentStorage->loadByProperties([
      'entity_type' => 'helpdesk_issue',
      'entity_id' => $localIssueId,
      'field_deleted' => 0,
    ]);
    $removeCommentIds = array_diff(array_keys($localComments), $remoteCommentIds);
    try {
      foreach ($removeCommentIds as $removeCommentId) {
        // Instead of removing them, mark them as deleted.
        $localComments[$removeCommentId]->set('field_deleted', 1)->save();
      }
    }
    catch (EntityStorageException $e) {
      throw new HelpdeskPluginException($e);
    }
  }

  /**
   * Remove access to existing items that no longer apply.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param \Drupal\user\UserInterface|null $user
   *   The user.
   * @param array $remoteIssueIds
   *   The IDs for remote issues.
   * @param int $lastSync
   *   Date as int of the last synchronization.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function removeAccessOfExistingItems(HelpdeskInterface $helpdesk, UserInterface|null $user, array $remoteIssueIds, int $lastSync): void {
    try {
      $issueStorage = $this->entityTypeManager->getStorage('helpdesk_issue');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      throw new HelpdeskPluginException($e);
    }
    $localIssues = [];
    foreach ($issueStorage->loadByProperties([
      'helpdesk' => $helpdesk->id(),
      'users' => $user->id(),
    ]) as $key => $issue) {
      /** @var \Drupal\helpdesk_integration\IssueInterface $issue */
      if ($issue->getChangedTime() > $lastSync) {
        $localIssues[$key] = $issue;
      }
    }
    $removeIssueIds = array_diff(array_keys($localIssues), $remoteIssueIds);
    foreach ($removeIssueIds as $removeIssueId) {
      $removeIssue = $localIssues[$removeIssueId];
      try {
        if ($removeIssue->removeUser($user->id())) {
          $removeIssue->save();
        }
      }
      catch (EntityStorageException $e) {
        throw new HelpdeskPluginException($e);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  final public function updateAllUsers(HelpdeskInterface $helpdesk, ?UserInterface $user = NULL, ?array &$batch = NULL): void {
    if (!$helpdesk->isUserSyncEnabled()) {
      return;
    }
    if ($user === NULL) {
      $uids = $this->service->getUidsWithAccess();
    }
    else {
      $uids = [];
      if ($user->isActive()) {
        $uids[] = (int) $user->id();
      }
    }

    if ($batch !== NULL) {
      $batch['operations'][] = [
        'helpdesk_integration_batch_push_users',
        [$helpdesk, $uids],
      ];
      return;
    }

    foreach ($uids as $uid) {
      try {
        /** @var \Drupal\user\UserInterface $helpdeskUser */
        $helpdeskUser = User::load($uid);
        $this->eventDispatcher->dispatch(self::getUserPreCreationEvent($helpdesk, $helpdeskUser));
        $remoteUserId = $this->pushUser($helpdesk, $helpdeskUser);
        $this->setUserData($helpdeskUser, $helpdesk, self::REMOTE_ID, $remoteUserId);
        $this->eventDispatcher->dispatch(self::getUserPostCreationEvent($helpdesk, $helpdeskUser, $remoteUserId));
      }
      catch (HelpdeskPluginException $e) {
        $this->logger->alert('Pushing users to @helpdesk failed: @exception', [
          '@helpdesk' => $helpdesk->label(),
          '@exception' => $e->getMessage(),
        ]);
      }
    }
  }

  /**
   * Gets the user post creation event.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param string $remoteId
   *   The ID of the remote issue.
   *
   * @return \Drupal\helpdesk_integration\UserPostCreationEvent
   *   The post creation event.
   */
  public static function getUserPostCreationEvent(HelpdeskInterface $helpdesk, UserInterface $user, string $remoteId): UserPostCreationEvent {
    $userPostCreationEvent = new UserPostCreationEvent();
    $userPostCreationEvent->setHelpdesk($helpdesk);
    $userPostCreationEvent->setUserId($user->id());
    $userPostCreationEvent->setRemoteUserId($remoteId);
    return $userPostCreationEvent;
  }

  /**
   * Gets the user pre creation event.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return \Drupal\helpdesk_integration\UserPreCreationEvent
   *   The user creation event.
   */
  public static function getUserPreCreationEvent(HelpdeskInterface $helpdesk, UserInterface $user): UserPreCreationEvent {
    $userPreCreationEvent = new UserPreCreationEvent();
    $userPreCreationEvent->setHelpdesk($helpdesk);
    $userPreCreationEvent->setUserId($user->id());
    return $userPreCreationEvent;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteUserId(HelpdeskInterface $helpdesk, UserInterface $user): string|NULL {
    $remote_id = $this->getUserData($user, $helpdesk, self::REMOTE_ID);
    if ($remote_id === NULL) {
      $remote_id = $this->pushUser($helpdesk, $user);
      $this->setUserData($user, $helpdesk, self::REMOTE_ID, $remote_id);
    }
    return $remote_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteUrl(HelpdeskInterface $helpdesk): ?string {
    return NULL;
  }

}
