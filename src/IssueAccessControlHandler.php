<?php

namespace Drupal\helpdesk_integration;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the helpdesk_issue entity.
 *
 * @see \Drupal\helpdesk_integration\Entity\Issue.
 */
class IssueAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\helpdesk_integration\IssueInterface $entity */
    if ($operation === 'view' && $account->hasPermission('access helpdesk')) {
      return AccessResult::allowedIf($entity->hasUser($account->id()));
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'access helpdesk');
  }

}
