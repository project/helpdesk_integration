<?php

namespace Drupal\helpdesk_integration;

use Drupal\Core\Access\AccessResult;
use Drupal\user\Entity\User;

/**
 * Handles the access of the helpdesk integrations.
 */
class Access {

  /**
   * Gets the access result for the available helpdesk instances.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Object to determine if access is allowed.
   */
  public static function availableHelpdesk(): AccessResult {
    return AccessResult::allowedIf(count(\Drupal::service('helpdesk_integration.service')->getHelpdeskInstances()) > 0);
  }

  /**
   * Gets the access result for the current user.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Object to determine if access is allowed.
   */
  public static function applicableHelpdeskForUser(): AccessResult {
    return AccessResult::allowedIf(count(\Drupal::service('helpdesk_integration.service')->getHelpdeskInstancesForUser(User::load(\Drupal::currentUser()->id()))) > 0);
  }

}
