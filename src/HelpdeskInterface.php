<?php

namespace Drupal\helpdesk_integration;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a helpdesk entity type.
 */
interface HelpdeskInterface extends ConfigEntityInterface {

  /**
   * Reads and prepares extra settings of helpdesk plugin.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface
   *   This helpdesk entity.
   */
  public function postLoadEntity(): HelpdeskInterface;

  /**
   * Determine if the helpdesk entity is the default one.
   *
   * @return bool
   *   TRUE, if this is the default, FALSE otherwise.
   */
  public function isDefault(): bool;

  /**
   * Determine if issues and comments should be synced directly.
   *
   * @return bool
   *   TRUE, if issues and comments should be synced directly, FALSE otherwise.
   */
  public function isDirectSyncEnabled(): bool;

  /**
   * Determine if additional information is added to an issue.
   *
   * @return bool
   *   TRUE, if additional information should be added, FALSE otherwise.
   */
  public function isContextInformation(): bool;

  /**
   * Determine if the users should be pushed to the helpdesk.
   *
   * @return bool
   *   TRUE, if the users should be pushed, FALSE otherwise.
   */
  public function isUserSyncEnabled(): bool;

  /**
   * The maximum file size of a single attachment.
   *
   * @return int
   *   The maximum file size.
   */
  public function getMaxFileSize(): int;

  /**
   * Gets the plugin for this helpdesk entity.
   *
   * @return \Drupal\helpdesk_integration\PluginInterface
   *   The plugin associated with this helpdesk instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getPlugin(): PluginInterface;

  /**
   * Determine if the given user has access to this helpdesk instance.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity for which to determine, if they had access.
   *
   * @return bool
   *   TRUE, if the user has access to this helpdesk instance, FALSE otherwise.
   */
  public function hasAccess(UserInterface $user): bool;

}
