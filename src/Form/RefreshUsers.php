<?php

namespace Drupal\helpdesk_integration\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Refreshes users to all external helpdesks.
 */
class RefreshUsers extends ConfirmFormBase {

  /**
   * The helpdesk integration service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): RefreshUsers {
    $instance = parent::create($container);
    $instance->setHelpdeskService($container
      ->get('helpdesk_integration.service'));
    return $instance;
  }

  /**
   * Sets the helpdesk integration service.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk integration service.
   */
  public function setHelpdeskService(Service $service): void {
    $this->service = $service;
  }

  /**
   * Gets the question to synchronize the users or not.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The markup.
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Do you want to synchronize all users with your helpdesk(s)?');
  }

  /**
   * Gets the URL to cancel.
   *
   * @return \Drupal\Core\Url
   *   The cancel URL.
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('entity.helpdesk.collection');
  }

  /**
   * Gets the form ID.
   *
   * @return string
   *   The form ID.
   */
  public function getFormId(): string {
    return 'helpdesk_refresh_users_confirm';
  }

  /**
   * Submits the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $batch = [
      'title' => $this->t('Synchronizing all users...'),
      'operations' => [],
    ];
    $this->service->sync('users', NULL, NULL, FALSE, $batch);
    batch_set($batch);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
