<?php

namespace Drupal\helpdesk_integration\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the helpdesk issue entity edit forms.
 */
class Issue extends ContentEntityForm {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * The helpdesk service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Issue {
    /** @var \Drupal\helpdesk_integration\Form\Issue $instance */
    $instance = parent::create($container);
    $instance->setRenderer($container->get('renderer'));
    $instance->routeMatch = $container->get('current_route_match');
    $instance->service = $container->get('helpdesk_integration.service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Exception
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = [
      '%label' => $this->entity->label(),
    ];
    $logger_arguments = $message_arguments + [
      'link' => $this->renderer->render($link),
    ];

    $this->messenger()->addStatus(
      $this->t('New helpdesk issue %label has been created.',
        $message_arguments));
    $this->logger('helpdesk_integration')
      ->notice('Created new helpdesk issue %label', $logger_arguments);

    $form_state->setRedirect('entity.helpdesk_issue.canonical', [
      'helpdesk_issue' => $entity->id(),
    ]);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#submit' => ['::submitForm', '::save'],
    ];
    if ($this->getRequest()->get('_wrapper_format') !== NULL) {
      $form['actions']['submit'] += [
        '#attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
        '#ajax' => [
          'callback' => [$this, 'closeModalForm'],
          'event' => 'click',
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($this->service->getDefaultHelpdesk()->isContextInformation()) {
      $form_state->getValue('body')[0]['value']
        .= $this->service->getContextInformation();
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Closes the modal dialog.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response.
   */
  public function closeModalForm(): AjaxResponse {
    $response = new AjaxResponse();
    foreach ($this->messenger->deleteAll() as $type => $type_messages) {
      /** @var string[]|\Drupal\Component\Render\MarkupInterface[] $type_messages */
      foreach ($type_messages as $message) {
        $response->addCommand(new MessageCommand((string) $message, NULL, ['type' => $type], FALSE));
      }
    }
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * Sets the renderer.
   *
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function setRenderer(Renderer $renderer): void {
    $this->renderer = $renderer;
  }

}
