<?php

namespace Drupal\helpdesk_integration\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class building the form of a helpdesk.
 *
 * @property \Drupal\helpdesk_integration\HelpdeskInterface $entity
 */
class Helpdesk extends EntityForm {

  /**
   * The helpdesk service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Helpdesk {
    $instance = parent::create($container);
    $instance->setHelpdeskService($container
      ->get('helpdesk_integration.service'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $plugins = $this->service->getPluginInstances();
    $isFirst = empty($this->service->getHelpdeskInstances());
    $options = [];
    foreach ($plugins as $plugin) {
      $options[$plugin->getPluginId()] = $plugin->label();
    }
    if ($this->entity->isNew()) {
      $default_plugin = count($options) === 1 ? key($options) : '';
      $this->entity->set('status', TRUE);
    }
    else {
      $default_plugin = $this->entity->get('plugin_id');
    }
    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Backend'),
      '#options' => $options,
      '#default_value' => $default_plugin,
      '#disabled' => !$this->entity->isNew() || count($options) === 1,
      '#required' => TRUE,
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the helpdesk.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\helpdesk_integration\Entity\Helpdesk::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
      '#disabled' => $this->entity->isDefault(),
      '#states' => [
        'disabled' => [
          ':input[name="default"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default'),
      '#default_value' => $this->entity->isDefault() || $isFirst,
      '#disabled' => $this->entity->isDefault() || $isFirst,
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['direct_sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Directly synchronize issues and comments'),
      '#default_value' => $this->entity->isDirectSyncEnabled(),
      '#description' => $this->t('If enabled, new issues and comments will be synchronized directly. Otherwise, they get queued and synchronized later in separate processes. If the initial direct synchronization fails, the item will be queued as well.'),
    ];
    $form['user_sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Synchronize Users'),
      '#default_value' => $this->entity->isUserSyncEnabled(),
      '#description' => $this->t('Synchronize users to helpdesk'),
    ];
    $form['context_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Context Information'),
      '#default_value' => $this->entity->isContextInformation(),
      '#description' => $this->t('This adds information like user data to the ticket.'),
    ];
    $form['max_file_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Max. file size for attachments (in MB)'),
      '#default_value' => $this->entity->getMaxFileSize(),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the helpdesk.'),
    ];

    $form['#tree'] = TRUE;

    foreach ($plugins as $plugin) {
      $plugin_id = $plugin->getPluginId();
      $required = [
        '#states' => [
          'required' => [
            'select[name="plugin_id"]' => ['value' => $plugin_id],
          ],
        ],
      ];
      $form['details_' . $plugin_id] = [
        '#type' => 'container',
        '#states' => [
          'visible' => [
            'select[name="plugin_id"]' => ['value' => $plugin_id],
          ],
        ],
      ] + $plugin->buildConfigurationForm($this->entity, $required);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    foreach ($this->entity->{'details_' . $this->entity->get('plugin_id')} as $key => $value) {
      $this->entity->set($key, $value);
    }

    foreach ($this->service->getPluginInstances() as $plugin) {
      unset($this->entity->{'details_' . $plugin->getPluginId()});
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    try {
      $defaultHelpdesk = $this->service->getDefaultHelpdesk();
    }
    catch (\Exception $e) {
      $defaultHelpdesk = NULL;
    }
    if ($this->entity->isDefault()) {
      $this->entity->set('status', TRUE);
    }
    $result = parent::save($form, $form_state);
    if ($defaultHelpdesk && $this->entity->isDefault() && $defaultHelpdesk->id() !== $this->entity->id()) {
      // We get a new default.
      $defaultHelpdesk
        ->set('default', FALSE)
        ->save();
      $this->messenger()->addStatus($this->t('Default helpdesk changed.'));
    }

    $message_args = ['%label' => $this->entity->label()];
    $message = $result === SAVED_NEW
      ? $this->t('Created new helpdesk %label.', $message_args)
      : $this->t('Updated helpdesk %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirect('entity.helpdesk.collection');

    if ($result === SAVED_NEW && $this->entity->isUserSyncEnabled()) {
      $batch = [
        'title' => $this->t('Synchronizing all users...'),
        'operations' => [],
      ];
      $this->service->sync('users', $this->entity, NULL, FALSE, $batch);
      batch_set($batch);
    }
    return $result;
  }

  /**
   * Sets the helpdesk service.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The service.
   */
  public function setHelpdeskService(Service $service): void {
    $this->service = $service;
  }

}
