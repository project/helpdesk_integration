<?php

namespace Drupal\helpdesk_integration;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\comment\CommentInterface;
use Drupal\helpdesk_integration\Entity\Helpdesk;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service class for the helpdesk integration.
 */
class Service {

  use StringTranslationTrait;

  public const DEFAULT_MAX_FILE_SIZE = 20;

  public const REQUEUE_DELAY = 10;

  /**
   * The plugin manager.
   *
   * @var \Drupal\helpdesk_integration\PluginManager
   */
  protected PluginManager $pluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Service constructor.
   *
   * @param \Drupal\helpdesk_integration\PluginManager $plugin_manager
   *   The helpdesk plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(PluginManager $plugin_manager, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, LoggerChannelInterface $logger, RequestStack $requestStack) {
    $this->pluginManager = $plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->requestStack = $requestStack;
  }

  /**
   * Gets the default max file size for an attachment.
   *
   * @return int
   *   The max file size.
   */
  public static function getDefaultMaxFileSize(): int {
    return self::DEFAULT_MAX_FILE_SIZE;
  }

  /**
   * Returns a list of active helpdesk entities.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface[]
   *   The list of active helpdesk instances.
   */
  public function getHelpdeskInstances(): array {
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface[] $instances */
    $instances = [];
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
    foreach (Helpdesk::loadMultiple() as $helpdesk) {
      if ($helpdesk->status()) {
        $instances[] = $helpdesk;
      }
    }
    return $instances;
  }

  /**
   * Returns a list of active helpdesks for select lists in forms.
   *
   * @return array
   *   List of active helpdesk labels indexed by their helpdesk ID.
   */
  public function getHelpdesksForSelect(): array {
    $result = [];
    foreach ($this->getHelpdeskInstances() as $helpdeskInstance) {
      $result[$helpdeskInstance->id()] = $helpdeskInstance->label();
    }
    return $result;
  }

  /**
   * Returns a list of active helpdesk entities for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity for whom the list should be built.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface[]
   *   The list of active helpdesk instances.
   */
  public function getHelpdeskInstancesForUser(UserInterface $user): array {
    $instances = [];
    foreach ($this->getHelpdeskInstances() as $helpdesk) {
      if ($helpdesk->hasAccess($user)) {
        $instances[] = $helpdesk;
      }
    }
    return $instances;
  }

  /**
   * Get a list of all installed helpdesk plugins.
   *
   * @return \Drupal\helpdesk_integration\PluginInterface[]
   *   The list of installed helpdesk plugins.
   */
  public function getPluginInstances(): array {
    $instances = [];
    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      try {
        $instances[$id] = $this->pluginManager->createInstance($id);
      }
      catch (PluginException $e) {
        $this->logger->error($e->getMessage());
      }
    }
    return $instances;
  }

  /**
   * Create and return a helpdesk plugin instance for the given ID.
   *
   * @param string $plugin_id
   *   The ID of the plugin which should be returned.
   *
   * @return \Drupal\helpdesk_integration\PluginInterface
   *   The helpdesk plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getPluginInstance(string $plugin_id): PluginInterface {
    /** @var \Drupal\helpdesk_integration\PluginInterface $instance */
    $instance = $this->pluginManager->createInstance($plugin_id);
    return $instance;
  }

  /**
   * Determine and return the correct helpdesk entity for the given issue.
   *
   * By running the rule engine, this determines the best matching helpdesk
   * entity. If there is only one helpdesk instance available for the issue
   * owner, that one will be selected. Otherwise, the default helpdesk will
   * be used.
   *
   * This rule engine is intended to be extended with more sophisticated
   * conditions in later versions of this module.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue entity for which the helpdesk should be determined.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface
   *   The determined helpdesk entity.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function getHelpdesk(IssueInterface $issue): HelpdeskInterface {
    $helpdesks = $this->getHelpdeskInstancesForUser($issue->getOwner());
    if (empty($helpdesks)) {
      throw new HelpdeskPluginException('Not allowed while no active helpdesk available.');
    }
    if (count($helpdesks) === 1) {
      return reset($helpdesks);
    }

    return $this->getDefaultHelpdesk();
  }

  /**
   * Find and return the default helpdesk entity.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface
   *   The default helpdesk entity.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function getDefaultHelpdesk(): HelpdeskInterface {
    foreach ($this->getHelpdeskInstances() as $helpdesk) {
      if ($helpdesk->isDefault()) {
        return $helpdesk;
      }
    }
    throw new HelpdeskPluginException('Default helpdesk not available.');
  }

  /**
   * Gets the context information.
   *
   * @return string
   *   The context information.
   */
  public function getContextInformation(): string {
    $request = $this->requestStack->getParentRequest();
    $contextInfo = '<br><br>URL: ' . $request->server->get('HTTP_REFERER');
    $contextInfo .= '<br>Timestamp: ' . date('Y-m-d H:i:s', time());
    $contextInfo .= '<br>User Account: ' . $request->getSchemeAndHttpHost()
      . '/user/' . $request->getSession()->get('uid');
    $contextInfo .= '<br>Session ID: ' . $request->getSession()->getId();
    $contextInfo .= '<br>Session Name: ' . $request->getSession()->getName();

    return $contextInfo;
  }

  /**
   * Find and return the helpdesk entity with the given id.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface
   *   The default helpdesk entity.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function getHelpdeskById(string $id): HelpdeskInterface {
    foreach ($this->getHelpdeskInstances() as $helpdesk) {
      if ($helpdesk->id() === $id) {
        return $helpdesk;
      }
    }
    throw new HelpdeskPluginException('Helpdesk with id '
      . $id . ' not available.');
  }

  /**
   * Mark an issue as resolved and submit change to remote helpdesk.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue entity to which should be marked as resolved.
   */
  public function resolveIssue(IssueInterface $issue): void {
    $helpdeskId = $issue->get('helpdesk')->value;
    if ($helpdeskId === NULL) {
      $this->messenger->addWarning($this->t('Issue can not be marked as resolved, please try again later.'));
      return;
    }
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
    $helpdesk = Helpdesk::load($helpdeskId);
    try {
      $helpdesk->getPlugin()->resolveIssue($helpdesk, $issue);
      $issue
        ->set('resolved', TRUE)
        ->save();
    }
    catch (HelpdeskPluginException | PluginException | EntityStorageException $e) {
      $this->messenger->addWarning($this->t('Ticket system currently not available'));
      $this->logger->error($e->getMessage());
    }

  }

  /**
   * Add the new comment to its issue on the remote helpdesk.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   The comment entity.
   * @param bool $firstAttempt
   *   Whether this is the first (=initial) attempt to synchronize the item.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   */
  public function addCommentToIssue(CommentInterface $comment, bool $firstAttempt = FALSE): void {
    if ($comment->bundle() !== 'helpdesk_issue_comment' || !empty($comment->get('field_extid')->value)) {
      return;
    }
    /** @var \Drupal\helpdesk_integration\IssueInterface $issue */
    $issue = $comment->getCommentedEntity();
    if (empty($issue->get('extid')->value)) {
      $message = 'Comment ' . $comment->id() . ' can not be synced before the issue.';
      if (!$firstAttempt) {
        $this->logger->warning($message);
      }
      throw new DelayedRequeueException(self::REQUEUE_DELAY, $message);
    }
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
    $helpdesk = Helpdesk::load($issue->get('helpdesk')->value);
    if ($firstAttempt && !$helpdesk->isDirectSyncEnabled()) {
      throw new DelayedRequeueException(self::REQUEUE_DELAY, 'Skipping direct attempt.');
    }
    try {
      if (!$helpdesk->getPlugin()->isUserLocked($helpdesk, $issue->getOwner())) {
        $helpdesk->getPlugin()->addCommentToIssue($helpdesk, $issue, $comment);
        $comment->save();
      }
    }
    catch (HelpdeskPluginException | PluginException | EntityStorageException $e) {
      if (!$firstAttempt) {
        $this->logger->alert($e->getMessage());
      }
      throw new DelayedRequeueException(self::REQUEUE_DELAY, $e->getMessage());
    }
  }

  /**
   * Helper function to update/sync objects with remote helpdesk.
   *
   * @param string $type
   *   The object type to update, can be "users" or "issues".
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity to/from which the update/sync should be performed.
   * @param \Drupal\user\UserInterface|null $user
   *   Optional, the user entity which is related to the update/sync.
   * @param bool $force
   *   Optional, if TRUE the update/sync is performed regardless of any
   *   configured timeouts between similar tasks.
   * @param array|null $batch
   *   If an array is given, this callback does NOT update users directly but
   *   adds a Batch API compatible operation to the array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  protected function updateAll(string $type, HelpdeskInterface $helpdesk, ?UserInterface $user = NULL, bool $force = FALSE, ?array &$batch = NULL): void {
    $plugin = $helpdesk->getPlugin();
    switch ($type) {
      case 'users':
        $plugin->updateAllUsers($helpdesk, $user, $batch);
        break;

      case 'issues':
        $plugin->updateAllIssues($helpdesk, $user, $force);
        break;
    }
  }

  /**
   * Helper function to update/sync objects with one or all remote helpdesk(s).
   *
   * @param string $type
   *   The object type to update, can be "users" or "issues".
   * @param \Drupal\helpdesk_integration\HelpdeskInterface|null $helpdesk
   *   Optional, the helpdesk entity to/from which the update/sync should be
   *   performed. If not provided, the update/sync will be performed with all
   *   active helpdesks.
   * @param \Drupal\user\UserInterface|null $user
   *   Optional, the user entity which is related to the update/sync.
   * @param bool $force
   *   Optional, if TRUE the update/sync is performed regardless of any
   *   configured timeouts between similar tasks.
   * @param array|null $batch
   *   If an array is given, this callback does NOT update users directly but
   *   adds a Batch API compatible operation to the array.
   */
  public function sync(string $type, ?HelpdeskInterface $helpdesk = NULL, ?UserInterface $user = NULL, bool $force = FALSE, ?array &$batch = NULL): void {
    $helpdesks = ($helpdesk === NULL) ?
      $this->getHelpdeskInstances() :
      [$helpdesk];

    foreach ($helpdesks as $helpdeskInstance) {
      if ($user !== NULL && !$helpdeskInstance->hasAccess($user)) {
        continue;
      }
      try {
        $this->updateAll($type, $helpdeskInstance, $user, $force, $batch);
        $this->messenger->addStatus($this->t('Synced @type with @name', [
          '@type' => $type,
          '@name' => $helpdeskInstance->label(),
        ]));
      }
      catch (HelpdeskPluginException | PluginException $e) {
        $this->messenger->addWarning($this->t('Ticket system currently not available'));
        $this->logger->error($e->getMessage());
      }
    }
  }

  /**
   * Helper function to sync a new issue to the upstream helpdesk.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The new issue.
   * @param bool $firstAttempt
   *   Whether this is the first (=initial) attempt to synchronize the item.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   */
  public function syncIssue(IssueInterface $issue, bool $firstAttempt = FALSE): void {
    if (empty($issue->get('extid')->value)) {
      try {
        $helpdesk = $this->getHelpdesk($issue);
        if ($firstAttempt && !$helpdesk->isDirectSyncEnabled()) {
          throw new DelayedRequeueException(self::REQUEUE_DELAY, 'Skipping direct attempt.');
        }
        $issue->set('helpdesk', $helpdesk->id());
        if (!$helpdesk->getPlugin()->isUserLocked($helpdesk, $issue->getOwner())) {
          $helpdesk->getPlugin()->createIssue($helpdesk, $issue);
          $issue->save();
        }
      }
      catch (HelpdeskPluginException | PluginException | EntityStorageException $e) {
        if (!$firstAttempt) {
          $this->logger->alert($e->getMessage());
        }
        throw new DelayedRequeueException(self::REQUEUE_DELAY, $e->getMessage());
      }
    }
  }

  /**
   * Provides a list of user IDs that are active and have access to helpdesk.
   *
   * @return int[]
   *   The user Ids.
   */
  public function getUidsWithAccess(): array {
    $roles = array_filter(Role::loadMultiple(), function ($role) {
      return $role->hasPermission('access helpdesk');
    });

    if (empty($roles)) {
      return [];
    }
    $result = [];
    try {
      $query = $this->entityTypeManager->getStorage('user')->getQuery()
        ->accessCheck(FALSE)
        ->condition('status', 1);
      if (!isset($roles[RoleInterface::AUTHENTICATED_ID])) {
        $query->condition('roles', array_keys($roles), 'IN');
      }
      foreach ($query->execute() as $uid) {
        $result[] = (int) $uid;
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger->error($e->getMessage());
    }
    return $result;
  }

}
