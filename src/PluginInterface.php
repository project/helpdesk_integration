<?php

namespace Drupal\helpdesk_integration;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\comment\CommentInterface;
use Drupal\user\UserInterface;

/**
 * Interface for helpdesk_integration plugins.
 */
interface PluginInterface extends PluginInspectionInterface {

  /**
   * Time in seconds in between no subsequent issue updates will be performed.
   */
  const REFRESH_INTERVAL = 600;

  const REMOTE_ID = 'remote_id';

  const HELPDESK_DIR = 'private://helpdesk';

  /**
   * Returns a string array with field names of the config form.
   *
   * @return string[]
   *   Array of strings with config field names.
   */
  public static function settingKeys(): array;

  /**
   * Adds plugin specific fields to the config form.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param array $required
   *   Specifies whether the form items of the plugins are required.
   *
   * @return array
   *   Form API array with fields to be added to the config form.
   */
  public function buildConfigurationForm(HelpdeskInterface $helpdesk, array $required): array;

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Find user entity related to given helpdesk which has given data stored.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param string $key
   *   The key representing the data record.
   * @param mixed $data
   *   The stored data record to search for.
   *
   * @return \Drupal\user\UserInterface|null
   *   The found user entity or NULL.
   */
  public function findUserByData(HelpdeskInterface $helpdesk, string $key, $data): ?UserInterface;

  /**
   * Get data stored for given user entity related to given helpdesk.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param string $key
   *   The key representing the data record.
   *
   * @return mixed
   *   The requested user account data.
   */
  public function getUserData(UserInterface $user, HelpdeskInterface $helpdesk, string $key): mixed;

  /**
   * Sets the user data.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   */
  public function setUserData(UserInterface $user, HelpdeskInterface $helpdesk, string $key, mixed $value): void;

  /**
   * Creates a new issue.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue entity that should be created remotely.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function createIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void;

  /**
   * Add new comment to an issue.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue entity to which the comment should be added.
   * @param \Drupal\comment\CommentInterface $comment
   *   The comment entity that should be created remotely.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function addCommentToIssue(HelpdeskInterface $helpdesk, IssueInterface $issue, CommentInterface $comment): void;

  /**
   * Mark an issue as resolved and submit change to remote helpdesk.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue entity to which should be marked as resolved.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function resolveIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void;

  /**
   * Updates all issues from the given helpdesk instance.
   *
   * This function is finally implemented by the PluginBase and can not be
   * overwritten by implementing plugins.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\user\UserInterface|null $user
   *   The user entity for which the update should be performed. If not
   *   provided, the user entity of the currently logged in user will be used.
   * @param bool $force
   *   If TRUE, the update will be performed in any case. Otherwise it will only
   *   be performed if the time since the last update for this user on the given
   *   helpdesk is longer than defined in self::REFRESH_INTERVAL.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function updateAllIssues(HelpdeskInterface $helpdesk, ?UserInterface $user = NULL, $force = FALSE): void;

  /**
   * Implemented by the backend plugin to receive all issues from helpdesk.
   *
   * The implementing plugin needs to determine, how to map Drupal user accounts
   * to helpdesk user accounts and needs to make sure that this mapping is
   * persistent. It is recommended to store the relevant data for this
   * mapping with the Drupal user by utilizing UserDataInterface. Use the
   * getUserData and setUserData of this interface for this.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\user\UserInterface $user
   *   The user entity for which the issues should be received.
   * @param int $since
   *   If a positive value is given, the helpdesk should only look for issues
   *   that have been created or updated after that timestamp, where the value
   *   of $since is the number of seconds since 1-1-1970.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface[]
   *   An array of issue entities that have been received but not saved.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function getAllIssues(HelpdeskInterface $helpdesk, UserInterface $user, int $since = 0): array;

  /**
   * Push given or all users to remote helpdesk.
   *
   * This function is finally implemented by the PluginBase and can not be
   * overwritten by implementing plugins.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\user\UserInterface|null $user
   *   The user that should be pushed to the helpdesk. If NULL, all users will
   *   be pushed. If a dedicated user record is provided, it must have been
   *   checked if that user has access to the given helpdesk.
   * @param array|null $batch
   *   If an array is given, this callback does NOT update users directly but
   *   adds a Batch API compatible operation to the array.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function updateAllUsers(HelpdeskInterface $helpdesk, ?UserInterface $user = NULL, ?array &$batch = NULL): void;

  /**
   * Implemented by the backend plugin to push a single user entity to helpdesk.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\user\UserInterface $user
   *   The user entity that should be pushed.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function pushUser(HelpdeskInterface $helpdesk, UserInterface $user): string;

  /**
   * Get the remote ID of a local user.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\user\UserInterface $user
   *   The user entity on behalf of which the client should communicate.
   *
   * @return string|null
   *   The remote user id or NULL otherwise.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function getRemoteUserId(HelpdeskInterface $helpdesk, UserInterface $user): string|NULL;

  /**
   * Checks whether the user in the helpdesk platform is locked.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return bool
   *   Whether the user is locked or not.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function isUserLocked(HelpdeskInterface $helpdesk, UserInterface $user): bool;

  /**
   * Returns the URL of the remote helpdesk.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   *
   * @return string|null
   *   The URL of the remote helpdesk or NULL, if this is not supported.
   */
  public function getRemoteUrl(HelpdeskInterface $helpdesk): ?string;

}
