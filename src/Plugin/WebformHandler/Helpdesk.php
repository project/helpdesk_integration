<?php

namespace Drupal\helpdesk_integration\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\helpdesk_integration\Entity\Issue;
use Drupal\helpdesk_integration\Service;
use Drupal\user\Entity\User;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create Helpdesk issue from a webform submission.
 *
 * @WebformHandler(
 *   id = "helpdesk_integration",
 *   label = @Translation("Helpdesk"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a webform submission to a helpdesk."),
 * )
 */
class Helpdesk extends WebformHandlerBase {

  /**
   * The helpdesk services.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, mixed $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->service = $container->get('helpdesk_integration.service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'helpdesk' => '',
      'field_email' => '',
      'field_subject' => '',
      'field_description' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $webform = $this->getWebform();
    $fields = [];
    foreach ($webform->getElementsInitializedAndFlattened() as $item) {
      $fields[$item['#webform_key']] = $item['#title'];
    }

    $form['helpdesk'] = [
      '#type' => 'select',
      '#title' => $this->t('Helpdesk'),
      '#options' => $this->service->getHelpdesksForSelect(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['helpdesk'],
    ];
    $form['field_email'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to store email address'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->configuration['field_email'],
    ];
    $form['field_subject'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to store subject'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->configuration['field_subject'],
    ];
    $form['field_description'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to store description'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->configuration['field_description'],
    ];

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE): void {
    if (!$update && $webform_submission->getState() === WebformSubmissionInterface::STATE_COMPLETED) {
      $data = $webform_submission->getData();
      $email = $data[$this->configuration['field_email']];
      $user = user_load_by_mail($email);
      if (!$user) {
        $user = user_load_by_name($email);
      }
      if (!$user) {
        $user = User::create([
          'name' => $email,
          'mail' => $email,
          'status' => TRUE,
        ]);
        $user->save();

        $user->block();
        $user->save();
      }

      $description = $data[$this->configuration['field_description']] . PHP_EOL . PHP_EOL . '<code>';
      foreach ($data as $key => $value) {
        if (!in_array($key, $this->configuration, TRUE)) {
          $description .= $key . ': ' . $value . PHP_EOL;
        }
      }
      $description .= '</code>';
      $issue = Issue::create([
        'helpdesk' => $this->configuration['helpdesk'],
        'uid' => $user->id(),
        'users' => [$user->id()],
        'title' => $data[$this->configuration['field_subject']],
        'body' => [
          'value' => $description,
          'format' => 'basic_html',
        ],
      ]);
      $issue->save();
    }
  }

}
