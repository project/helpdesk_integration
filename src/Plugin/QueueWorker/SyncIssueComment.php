<?php

namespace Drupal\helpdesk_integration\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\comment\Entity\Comment;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Helpdesk Issue Comment queue worker.
 *
 * @QueueWorker(
 *   id = "helpdesk_integration_sync_issue_comment",
 *   title = @Translation("Helpdesk Integration: Sync Issue Comment")
 * )
 */
final class SyncIssueComment extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The helpdesk service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Service $service, LoggerChannelInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->service = $service;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SyncIssueComment {
    return new SyncIssueComment(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('helpdesk_integration.service'),
      $container->get('logger.channel.helpdesk')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $comment = Comment::load($data);
    if ($comment === NULL) {
      $this->logger->alert('Issue comment for ID ' . $data . ' cannot be loaded!');
      return;
    }
    $this->service->addCommentToIssue($comment);
  }

}
