<?php

namespace Drupal\helpdesk_integration;

/**
 * The event class before a remote user gets created.
 */
class UserPreCreationEvent {

  const USER_PRE_CREATION_EVENT = 'helpdesk_integration.user_pre_creation_event';

  /**
   * The user ID.
   *
   * @var mixed
   */
  protected mixed $userId;

  /**
   * The helpdesk.
   *
   * @var \Drupal\helpdesk_integration\HelpdeskInterface
   */
  protected HelpdeskInterface $helpdesk;

  /**
   * Gets the helpdesk.
   *
   * @return HelpdeskInterface
   *   The helpdesk.
   */
  public function getHelpdesk(): HelpdeskInterface {
    return $this->helpdesk;
  }

  /**
   * Sets the helpdesk.
   *
   * @param HelpdeskInterface $helpdesk
   *   The helpdesk.
   */
  public function setHelpdesk(HelpdeskInterface $helpdesk): void {
    $this->helpdesk = $helpdesk;
  }

  /**
   * Gets the user ID.
   *
   * @return int
   *   The user ID.
   */
  public function getUserId(): int {
    return $this->userId;
  }

  /**
   * Sets the user ID.
   *
   * @param int $userId
   *   The user ID.
   */
  public function setUserId(int $userId): void {
    $this->userId = $userId;
  }

}
