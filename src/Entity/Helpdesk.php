<?php

namespace Drupal\helpdesk_integration\Entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\helpdesk_integration\HelpdeskInterface;
use Drupal\helpdesk_integration\PluginInterface;
use Drupal\helpdesk_integration\Service;
use Drupal\user\UserInterface;

/**
 * Defines the helpdesk entity type.
 *
 * @ConfigEntityType(
 *   id = "helpdesk",
 *   label = @Translation("Helpdesk"),
 *   label_collection = @Translation("Helpdesks"),
 *   label_singular = @Translation("helpdesk"),
 *   label_plural = @Translation("helpdesks"),
 *   label_count = @PluralTranslation(
 *     singular = "@count helpdesk",
 *     plural = "@count helpdesks",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\helpdesk_integration\HelpdeskAccessControlHandler",
 *     "list_builder" = "Drupal\helpdesk_integration\HelpdeskListBuilder",
 *     "form" = {
 *       "add" = "Drupal\helpdesk_integration\Form\Helpdesk",
 *       "edit" = "Drupal\helpdesk_integration\Form\Helpdesk",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "helpdesk",
 *   admin_permission = "administer helpdesk",
 *   links = {
 *     "collection" = "/admin/config/services/helpdesk",
 *     "add-form" = "/admin/config/services/helpdesk/add",
 *     "edit-form" = "/admin/config/services/helpdesk/{helpdesk}/edit",
 *     "delete-form" = "/admin/config/services/helpdesk/{helpdesk}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "status",
 *     "default",
 *     "direct_sync",
 *     "user_sync",
 *     "context_info",
 *     "max_file_size",
 *     "plugin_id",
 *     "plugin_settings"
 *   }
 * )
 */
class Helpdesk extends ConfigEntityBase implements HelpdeskInterface {

  /**
   * The helpdesk ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The helpdesk label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The helpdesk description.
   *
   * @var string
   */
  protected string $description;

  /**
   * Default indicator of helpdesk, exactly one active helpdesk has to be TRUE.
   *
   * @var bool
   */
  protected ?bool $default = NULL;

  /**
   * The helpdesk status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The helpdesk direct synchronization flag.
   *
   * @var bool
   */
  protected bool $direct_sync = FALSE;

  /**
   * The helpdesk user synchronization flag.
   *
   * @var bool
   */
  protected bool $user_sync = TRUE;

  /**
   * The maximum file size of a single attachment.
   *
   * @var int
   */
  protected int $max_file_size = Service::DEFAULT_MAX_FILE_SIZE;

  /**
   * The helpdesk context information flag.
   *
   * @var bool
   */
  protected bool $context_info = FALSE;

  /**
   * The ID of the plugin handling this integration.
   *
   * @var string
   */
  protected string $plugin_id;

  /**
   * The ID of the plugin handling this integration.
   *
   * @var string
   */
  protected string $plugin_settings;

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {
    $values['status'] = TRUE;
    if (empty(\Drupal::service('helpdesk_integration.service')->getHelpdeskInstances())) {
      $values['default'] = TRUE;
    }
    return parent::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if ($this->isSyncing()) {
      return;
    }
    $settings = [];
    foreach ($this->getPlugin()::settingKeys() as $settingKey) {
      $settings[$settingKey] = $this->get($settingKey);
    }
    $this->plugin_settings = json_encode($settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities): void {
    parent::postLoad($storage, $entities);
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $entity */
    foreach ($entities as $entity) {
      $entity->postLoadEntity();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postLoadEntity(): HelpdeskInterface {
    try {
      $plugin = $this->getPlugin();
      $settings = json_decode($this->plugin_settings, TRUE);
      foreach ($plugin::settingKeys() as $settingKey) {
        $this->set($settingKey, $settings[$settingKey]);
      }
    }
    catch (PluginException $e) {
      // Can be ignored.
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isDefault(): bool {
    return (bool) $this->default;
  }

  /**
   * {@inheritdoc}
   */
  public function isDirectSyncEnabled(): bool {
    return $this->direct_sync;
  }

  /**
   * {@inheritdoc}
   */
  public function isUserSyncEnabled(): bool {
    return $this->user_sync;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxFileSize(): int {
    return $this->max_file_size;
  }

  /**
   * {@inheritdoc}
   */
  public function isContextInformation(): bool {
    return $this->context_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): PluginInterface {
    /** @var \Drupal\helpdesk_integration\Service $service */
    $service = \Drupal::service('helpdesk_integration.service');
    return $service->getPluginInstance($this->plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccess(UserInterface $user): bool {
    return $user->hasPermission('access helpdesk');
  }

}
