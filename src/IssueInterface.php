<?php

namespace Drupal\helpdesk_integration;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a helpdesk issue entity type.
 */
interface IssueInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the helpdesk issue title.
   *
   * @return string
   *   Title of the helpdesk issue.
   */
  public function getTitle(): string;

  /**
   * Sets the helpdesk issue title.
   *
   * @param string $title
   *   The helpdesk issue title.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface
   *   The called helpdesk issue entity.
   */
  public function setTitle(string $title): IssueInterface;

  /**
   * Gets the helpdesk issue body.
   *
   * @return string
   *   Body of the helpdesk issue.
   */
  public function getBody(): string;

  /**
   * Gets the helpdesk issue creation timestamp.
   *
   * @return int
   *   Creation timestamp of the helpdesk issue.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the helpdesk issue creation timestamp.
   *
   * @param int $timestamp
   *   The helpdesk issue creation timestamp.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface
   *   The called helpdesk issue entity.
   */
  public function setCreatedTime(int $timestamp): IssueInterface;

  /**
   * Gets all issue attachments.
   *
   * @return array
   *   The attachments.
   */
  public function getIssueAttachments(): array;

  /**
   * Adds an attachment to an issue.
   *
   * @param string $file_name
   *   The filename.
   * @param string $url
   *   The URL.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface
   *   The issue interface.
   */
  public function addIssueAttachment(string $file_name, string $url): IssueInterface;

  /**
   * Add a comment to this issue during remote sync.
   *
   * @param string $extid
   *   The external ID of the comment.
   * @param string $body
   *   The comment body.
   * @param \Drupal\user\UserInterface $user
   *   The user entity of the comment author.
   * @param int $created
   *   The timestamp when this comment was created.
   * @param int $changed
   *   The timestamp when this comment was changed.
   *
   * @return int
   *   The index of this comment in the comments array.
   */
  public function addComment(string $extid, string $body, UserInterface $user, int $created, int $changed): int;

  /**
   * Add an attachment to a comment during remote sync.
   *
   * @param int $comment_id
   *   The index of this comment in the comments array.
   * @param string $file_name
   *   The filename of the attachment.
   * @param string $url
   *   The url from where to load the attachment's content.
   * @param array $download_headers
   *   The request headers to load the attachment's content.
   * @param string $method
   *   The request method.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface
   *   This issue entity.
   */
  public function addCommentAttachment(int $comment_id, string $file_name, string $url, array $download_headers = [], string $method = 'get'): IssueInterface;

  /**
   * Get the list of comments during remote sync.
   *
   * @return array
   *   The list of comments.
   */
  public function getComments(): array;

  /**
   * Determine if a user is contained in the access list for this issue.
   *
   * @param int $uid
   *   The id of the user entity to check.
   *
   * @return bool
   *   TRUE if the user is on the list, FALSE otherwise.
   */
  public function hasUser(int $uid): bool;

  /**
   * Add a user to the access list for this issue if necessary.
   *
   * @param int $uid
   *   The id of the user entity to add.
   *
   * @return bool
   *   TRUE if the user had to be added, FALSE if that user has already been on
   *   the list.
   */
  public function addUser(int $uid): bool;

  /**
   * Remove a user from the access list for this issue if necessary.
   *
   * @param int $uid
   *   The id of the user entity to remove.
   *
   * @return bool
   *   TRUE if the user had to be removed, FALSE if that user has not been on
   *   the list.
   */
  public function removeUser(int $uid): bool;

}
