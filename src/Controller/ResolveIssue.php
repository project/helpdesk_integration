<?php

namespace Drupal\helpdesk_integration\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\helpdesk_integration\IssueInterface;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Refreshes issues from all external helpdesks.
 */
class ResolveIssue extends ControllerBase {

  /**
   * The helpdesk integration service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ResolveIssue {
    $instance = parent::create($container);
    $instance->setHelpdeskService($container
      ->get('helpdesk_integration.service'));
    return $instance;
  }

  /**
   * Sets the helpdesk integration service.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk integration service.
   */
  public function setHelpdeskService(Service $service): void {
    $this->service = $service;
  }

  /**
   * Checks the access, if a user can resolve an issue.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $helpdesk_issue
   *   The issue entity for which access should be checked.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Object to determine if access is allowed.
   */
  public static function access(IssueInterface $helpdesk_issue): AccessResult {
    return AccessResult::allowedIf(!$helpdesk_issue->get('resolved')->value &&
      $helpdesk_issue->hasUser(\Drupal::currentUser()->id()));
  }

  /**
   * Builds the response.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $helpdesk_issue
   *   The issue entity which should be resolved.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Destination when completed.
   */
  public function build(IssueInterface $helpdesk_issue): RedirectResponse {
    $this->service->resolveIssue($helpdesk_issue);
    return new RedirectResponse(Url::fromRoute('entity.helpdesk_issue.canonical', [
      'helpdesk_issue' => $helpdesk_issue->id(),
    ])->toString());
  }

}
