<?php

namespace Drupal\helpdesk_integration\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\helpdesk_integration\HelpdeskPluginException;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Refreshes issues from all external helpdesk integrations.
 */
class RefreshIssues extends ControllerBase {

  /**
   * The helpdesk integration service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): RefreshIssues {
    $instance = parent::create($container);
    $instance->setService($container->get('helpdesk_integration.service'));
    return $instance;
  }

  /**
   * Builds the response.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Destination when completed.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function build(): RedirectResponse {
    try {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->entityTypeManager()->getStorage('user')
        ->load($this->currentUser()->id());
      $this->service->sync('issues', NULL, $user, TRUE);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      throw new HelpdeskPluginException($e);
    }
    return new RedirectResponse(Url::fromRoute('helpdesk_integration.helpdesk')->toString());
  }

  /**
   * Sets the helpdesk service.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The service class.
   */
  public function setService(Service $service): void {
    $this->service = $service;
  }

}
