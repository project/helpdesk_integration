<?php

namespace Drupal\helpdesk_integration;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Handles the control of access for the helpdesk integration.
 *
 * @package Drupal\helpdesk_integration
 */
class HelpdeskAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $entity */
    if ($operation === 'delete' && $entity->isDefault()) {
      return AccessResult::forbidden('Default helpdesk can not be deleted.');
    }
    return parent::checkAccess($entity, $operation, $account);
  }

}
