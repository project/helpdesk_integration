<?php

namespace Drupal\helpdesk_integration\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\helpdesk_integration\Service;
use Drush\Attributes\Command;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drush command file.
 */
class SyncCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The helpdesk services.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected Service $service;

  /**
   * SyncCommands constructor.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk services.
   */
  public function __construct(Service $service) {
    parent::__construct();
    $this->service = $service;
  }

  /**
   * Return an instance of these Drush commands.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\helpdesk_integration\Drush\Commands\SyncCommands
   *   The instance of Drush commands.
   */
  public static function create(ContainerInterface $container): SyncCommands {
    return new self(
      $container->get('helpdesk_integration.service'),
    );
  }

  /**
   * List all configured helpdesk instances.
   */
  #[Command(name: 'helpdesk:list', aliases: [])]
  #[Usage(name: 'helpdesk:list', description: 'List all configured helpdesk instances.')]
  public function list(): RowsOfFields {
    $rows = [];
    foreach ($this->service->getHelpdeskInstances() as $helpdesk) {
      $rows[] = [
        'id' => $helpdesk->id(),
        'label' => $helpdesk->label(),
        'status' => $helpdesk->status() ? $this->t('enabled')
          : $this->t('disabled'),
        'default' => $helpdesk->isDefault() ? $this->t('default') : '',
      ];
    }

    return new RowsOfFields($rows);
  }

  /**
   * Synchronize/push user accounts to all configured helpdesk instances.
   */
  #[Command(name: 'helpdesk:sync:users', aliases: [])]
  #[Usage(name: 'helpdesk:sync:users', description: 'Synchronize/push user accounts to all configured helpdesk instances.')]
  public function syncUsers(): void {
    $this->service->sync('users');
  }

}
