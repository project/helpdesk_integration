<?php

namespace Drupal\helpdesk_integration;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event class after successfully created user.
 *
 * @package Drupal\helpdesk_integration
 */
class UserPostCreationEvent extends Event {

  /**
   * The user ID.
   *
   * @var mixed
   */
  protected mixed $userId;

  /**
   * The remote user ID.
   *
   * @var string
   */
  protected string $remoteUserId;

  /**
   * The helpdesk.
   *
   * @var \Drupal\helpdesk_integration\HelpdeskInterface
   */
  protected HelpdeskInterface $helpdesk;

  /**
   * Gets the helpdesk.
   *
   * @return HelpdeskInterface
   *   The helpdesk.
   */
  public function getHelpdesk(): HelpdeskInterface {
    return $this->helpdesk;
  }

  /**
   * Sets the helpdesk.
   *
   * @param HelpdeskInterface $helpdesk
   *   The helpdesk.
   */
  public function setHelpdesk(HelpdeskInterface $helpdesk): void {
    $this->helpdesk = $helpdesk;
  }

  /**
   * Gets the user ID.
   *
   * @return mixed
   *   The user ID.
   */
  public function getUserId(): mixed {
    return $this->userId;
  }

  /**
   * Sets the user ID.
   *
   * @param mixed $userId
   *   The user ID.
   */
  public function setUserId(mixed $userId): void {
    $this->userId = $userId;
  }

  /**
   * Gets the remote user ID.
   *
   * @return string
   *   The remote user ID.
   */
  public function getRemoteUserId(): string {
    return $this->remoteUserId;
  }

  /**
   * Sets the remote user ID.
   *
   * @param string $remoteUserId
   *   The remote user ID.
   */
  public function setRemoteUserId(string $remoteUserId): void {
    $this->remoteUserId = $remoteUserId;
  }

}
